# Première étape : Créer l'image de base
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

# Deuxième étape : Construire l'application
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src

# Copier les fichiers nécessaires pour la restauration des dépendances
COPY ["dockerfile", "."]
COPY ["VeraxShield/VeraxShield/VeraxShield.csproj", "VeraxShield/"]
RUN dotnet restore "VeraxShield/VeraxShield.csproj"

# Copier le reste des fichiers
COPY . .

WORKDIR "/src/VeraxShield/VeraxShield"
RUN dotnet build "VeraxShield.csproj" -c Release -o /app/build

# Troisième étape : Publier l'application
FROM build AS publish
RUN dotnet publish "VeraxShield.csproj" -c Release -o /app/publish

# Quatrième étape : Créer l'image finale
FROM base AS final
WORKDIR /app

# Copier les fichiers publiés dans l'image finale
COPY --from=publish /app/publish .

# Définir le point d'entrée de l'application
ENTRYPOINT ["dotnet", "VeraxShield.dll"]
