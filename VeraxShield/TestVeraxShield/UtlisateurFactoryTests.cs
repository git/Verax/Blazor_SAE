namespace TestVeraxShield.factoriesTests;

using Xunit;
using VeraxShield.factories;
using VeraxShield.composants.formulaires.modeles;
using VeraxShield.modele.utilisateurs;

public class UtilisateursFactoryTests
{
    [Fact]
    public void ConvertsToUtilisateur_WithNewPasswordHashing()
    {
        // Arrange
        var modele = new FormulaireAjoutModele { Mdp = "newPassword", /* other properties */ };

        // Act
        var utilisateur = UtilisateursFactory.toUtilisateur(modele);
        
        // Assert
        Assert.NotEqual("newPassword", utilisateur.Mdp);
    }

    [Fact]
    public void ConvertsToModele_FromUtilisateur()
    {
        // Arrange
        var utilisateur = new Utilisateur("pseudo", "nom", "prenom", "role", "mdp", "mail", false);
        
        // Act
        var modele = UtilisateursFactory.toModele(utilisateur);
        
        // Assert
    }

    
    
    
    [Fact]
    public void ConvertsToModele_FromUtilisateurs()
    {
        // Arrange
        var utilisateur = new Utilisateur("pseudo", "nom", "prenom", "role", "mdp", "mail", false);
        
        // Act
        var modele = UtilisateursFactory.toModele(utilisateur);
        
        // Assert
        Assert.Equal(utilisateur.Pseudo, modele.Pseudo);
        Assert.Equal(utilisateur.Nom, modele.Nom);
        Assert.Equal(utilisateur.Prenom, modele.Prenom);
        Assert.Equal(utilisateur.Role, modele.Role);
        Assert.Equal(utilisateur.Mail, modele.Mail);
        Assert.Equal(utilisateur.IsBan, modele.IsBan);
    }

}
