using Moq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using VeraxShield.composants.formulaires.modeles;
using VeraxShield.modele.utilisateurs;
using VeraxShield.services.UtilisateursDataService;
using Xunit;

namespace VeraxShield.UnitTests
{
    public class DonneurEtatTests
    {
        private readonly DonneurEtat _donneurEtat;
        private readonly Mock<IAuthentificationService> _mockAuthService;
        private readonly Mock<IUtilisateursDataService> _mockUserDataService;

        public DonneurEtatTests()
        {
            _mockAuthService = new Mock<IAuthentificationService>();
            _mockUserDataService = new Mock<IUtilisateursDataService>();
            _donneurEtat = new DonneurEtat(_mockAuthService.Object, _mockUserDataService.Object);
        }

        [Fact]
        public async Task Connexion_ValidCredentials_SetsCurrentUser()
        {
            // Arrange
            var requeteConnexion = new RequeteConnexion { Pseudo = "testUser", MotDePasse = "testPass" };
            var utilisateurCourant = new UtilisateurCourant
            {
                Pseudo = "testUser",
                EstAuthentifie = true,
                Claims = new Dictionary<string, string> { { ClaimTypes.Role, "User" } }
            };

            _mockAuthService.Setup(x => x.GetUtilisateur(requeteConnexion.Pseudo)).ReturnsAsync(utilisateurCourant);
            _mockAuthService.Setup(x => x.Connexion(requeteConnexion)).Returns(Task.CompletedTask);

            // Act
            await _donneurEtat.Connexion(requeteConnexion);

            // Assert
            Assert.NotNull(_donneurEtat._utilisateurCourant);
            Assert.True(_donneurEtat._utilisateurCourant.EstAuthentifie);
            _mockAuthService.Verify(x => x.Connexion(requeteConnexion), Times.Once);
            _mockAuthService.Verify(x => x.GetUtilisateur(requeteConnexion.Pseudo), Times.Once);
        }

        [Fact]
        public async Task Deconnexion_ClearsCurrentUser()
        {
            // Arrange - assume user is logged in
            _donneurEtat._utilisateurCourant = new UtilisateurCourant { EstAuthentifie = true };

            // Act
            await _donneurEtat.Deconnexion();

            // Assert
            Assert.Null(_donneurEtat._utilisateurCourant);
        }

        [Fact]
        public async Task Inscription_ValidData_RegistersUser()
        {
            // Arrange
            var requeteInscription = new RequeteInscription
            {
                Pseudo = "newUser",
                MotDePasse = "newPass",
                Mail = "newUser@test.com",
                Nom = "New",
                Prenom = "User"
            };

            _mockAuthService.Setup(x => x.Inscription(requeteInscription)).Returns(Task.CompletedTask);

            // Act
            await _donneurEtat.Inscription(requeteInscription);

            // Assert - Since Inscription does not automatically log in the user, we check if the method was called.
            _mockAuthService.Verify(x => x.Inscription(requeteInscription), Times.Once);
        }
    }
}
