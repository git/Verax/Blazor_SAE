using Moq;
using VeraxShield.modele.utilisateurs;
using VeraxShield.services.UtilisateursDataService;

namespace TestVeraxShield;

public class IUtilisateursDataServiceTests
{
    private readonly Mock<IUtilisateursDataService> _service = new Mock<IUtilisateursDataService>();

    [Fact]
    public async Task AjouterUtilisateur_AddsUserSuccessfully()
    {
        // Arrange
        var user = new Utilisateur("testUser", "User", "Test", "dez", "password", "User", false);        
        _service.Setup(s => s.AjouterUtilisateur(It.IsAny<Utilisateur>()))
            .Returns(Task.CompletedTask)
            .Callback<Utilisateur>(u => Assert.Equal("testUser", u.Pseudo));

        // Act
        await _service.Object.AjouterUtilisateur(user);

        // Assert is handled by the Callback
    }
}