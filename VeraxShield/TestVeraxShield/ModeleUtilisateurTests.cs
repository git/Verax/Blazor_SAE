using VeraxShield.modele.utilisateurs;

namespace TestVeraxShield;

public class ModeleUtilisateurTests
{
    [Fact]
    public void ConstructorAssignsPropertiesCorrectly()
    {
        // Arrange
        string expectedPseudo = "TestPseudo";
        string expectedNom = "TestNom";
        string expectedPrenom = "TestPrenom";
        string expectedRole = "TestRole";
        string expectedMdp = "TestMdp";
        string expectedMail = "test@mail.com";
        bool expectedIsBan = true;

        // Act
        Utilisateur utilisateur = new Utilisateur(expectedPseudo, expectedNom, expectedPrenom, expectedRole, expectedMdp, expectedMail, expectedIsBan);

        // Assert
        Assert.Equal(expectedPseudo, utilisateur.Pseudo);
        Assert.Equal(expectedNom, utilisateur.Nom);
        Assert.Equal(expectedPrenom, utilisateur.Prenom);
        Assert.Equal(expectedRole, utilisateur.Role);
        Assert.Equal(expectedMdp, utilisateur.Mdp);
        Assert.Equal(expectedMail, utilisateur.Mail);
        Assert.Equal(expectedIsBan, utilisateur.IsBan);
    }

    [Theory]
    [InlineData("NewPseudo", "NewNom", "NewPrenom", "NewRole", "NewMdp", "new@mail.com", false)]
    [InlineData("AnotherPseudo", "AnotherNom", "AnotherPrenom", "AnotherRole", "AnotherMdp", "another@mail.com", true)]
    public void PropertiesUpdateCorrectly(string pseudo, string nom, string prenom, string role, string mdp, string mail, bool isBan)
    {
        // Arrange
        Utilisateur utilisateur = new Utilisateur(pseudo, nom, prenom, role, mdp, mail, isBan);

        // Act - changing values to test setter
        string updatedPseudo = pseudo + "Update";
        string updatedNom = nom + "Update";
        string updatedPrenom = prenom + "Update";
        string updatedRole = role + "Update";
        string updatedMdp = mdp + "Update";
        string updatedMail = "updated@" + mail;
        bool updatedIsBan = !isBan;

        utilisateur.Pseudo = updatedPseudo;
        utilisateur.Nom = updatedNom;
        utilisateur.Prenom = updatedPrenom;
        utilisateur.Role = updatedRole;
        utilisateur.Mdp = updatedMdp;
        utilisateur.Mail = updatedMail;
        utilisateur.IsBan = updatedIsBan;

        // Assert
        Assert.Equal(updatedPseudo, utilisateur.Pseudo);
        Assert.Equal(updatedNom, utilisateur.Nom);
        Assert.Equal(updatedPrenom, utilisateur.Prenom);
        Assert.Equal(updatedRole, utilisateur.Role);
        Assert.Equal(updatedMdp, utilisateur.Mdp);
        Assert.Equal(updatedMail, utilisateur.Mail);
        Assert.Equal(updatedIsBan, utilisateur.IsBan);
    }
}
