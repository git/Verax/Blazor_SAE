namespace TestVeraxShield;

public class ModeleAppUtilisateurTests
{
    
    [Fact]
    public void Constructeur_DoitInitialiserProprietes()
    {
        // Arrange & Act
        var utilisateur = new ModeleAppUtilisateur("pseudoTest", "NomTest", "PrenomTest", "email@test.com", "motdepasse", "RoleTest");

        // Assert
        Assert.Equal("pseudoTest", utilisateur.Pseudo);
        Assert.Equal("NomTest", utilisateur.Nom);
        Assert.Equal("PrenomTest", utilisateur.Prenom);
        Assert.Equal("email@test.com", utilisateur.Mail);
        Assert.Equal("motdepasse", utilisateur.MotDePasse);
        Assert.Contains("RoleTest", utilisateur.Roles);
    }

    [Fact]
    public void AjouterRole_DoItAjouterNouveauRole()
    {
        // Arrange
        var utilisateur = new ModeleAppUtilisateur("pseudoTest", "NomTest", "PrenomTest", "email@test.com", "motdepasse", "RoleTest");

        // Act
        utilisateur.ajouterRole("NouveauRole");

        // Assert
        Assert.Contains("NouveauRole", utilisateur.Roles);
    }

    [Fact]
    public void SupprimerRole_DoItSupprimerRoleExistant()
    {
        // Arrange
        var utilisateur = new ModeleAppUtilisateur("pseudoTest", "NomTest", "PrenomTest", "email@test.com", "motdepasse", "RoleTest");
        utilisateur.ajouterRole("NouveauRole");

        // Act
        utilisateur.supprimerRole("RoleTest");

        // Assert
        Assert.DoesNotContain("RoleTest", utilisateur.Roles);
    }
}