using System.Security.Claims;
using Moq;
using VeraxShield.composants.formulaires.modeles;
using VeraxShield.modele.utilisateurs;
using VeraxShield.services.UtilisateursDataService;

namespace TestVeraxShield;

public class AuthentificationServiceTests
{
     private readonly AuthentificationService _authService;
    private readonly Mock<IUtilisateursDataService> _mockDataService = new Mock<IUtilisateursDataService>();

    public AuthentificationServiceTests()
    {
        _authService = new AuthentificationService(_mockDataService.Object);
    }

    [Fact]
    public async Task Connexion_WithValidCredentials_ShouldSucceed()
    {
        // Arrange
        var testUser = new Utilisateur("testUser", "Test", "User", "test@user.com", BCrypt.Net.BCrypt.HashPassword("password"), "User", false);

        _mockDataService.Setup(s => s.getAllUtilisateurs())
            .ReturnsAsync(new List<Utilisateur> { testUser });

        var requete = new RequeteConnexion
        {
            Pseudo = "testUser",
            MotDePasse = "password"
        };

        // Act & Assert
        await _authService.Connexion(requete);
    }

    [Fact]
    public async Task Connexion_WithInvalidCredentials_ShouldThrowException()
    {
        // Arrange
        _mockDataService.Setup(s => s.getAllUtilisateurs())
            .ReturnsAsync(new List<Utilisateur>());

        var requete = new RequeteConnexion
        {
            Pseudo = "nonExistentUser",
            MotDePasse = "wrongPassword"
        };

        // Act & Assert
        await Assert.ThrowsAsync<Exception>(() => _authService.Connexion(requete));
    }

    [Fact]
    public async Task Inscription_ShouldCreateNewUser()
    {
        // Arrange
        var requete = new RequeteInscription
        {
            Pseudo = "newUser",
            Nom = "New",
            Prenom = "User",
            Mail = "new@user.com",
            MotDePasse = "newPassword"
        };

        // Setup the mock to verify that AjouterUtilisateur is called with a Utilisateur that matches the inscription details
        _mockDataService.Setup(s => s.AjouterUtilisateur(It.IsAny<Utilisateur>()))
            .Returns(Task.CompletedTask)
            .Callback<Utilisateur>(u =>
            {
                Assert.Equal(requete.Pseudo, u.Pseudo);
                Assert.True(BCrypt.Net.BCrypt.Verify(requete.MotDePasse, u.Mdp));
            });

        // Act
        await _authService.Inscription(requete);

    }
    [Fact]
    public async Task GetUtilisateur_ReturnsCorrectUser_WithClaims()
    {
        // Arrange
        var expectedPseudo = "testUser";
        var expectedRole = "User";
        var testUser = new Utilisateur("testUser", "Test", "User", "test@user.com", BCrypt.Net.BCrypt.HashPassword("password"), "User", false);
        var mockDataService = new Mock<IUtilisateursDataService>();
        mockDataService.Setup(s => s.getAllUtilisateurs())
            .ReturnsAsync(new List<Utilisateur> { testUser });

        var authService = new AuthentificationService(mockDataService.Object);

        // Act
        var utilisateurCourant = await authService.GetUtilisateur(expectedPseudo);
        utilisateurCourant.Claims.Add(ClaimTypes.Email, "test@user.com");

        // Assert
        Assert.NotNull(utilisateurCourant);
        Assert.True(utilisateurCourant.EstAuthentifie);
        Assert.Equal(expectedPseudo, utilisateurCourant.Pseudo);
        
    }

}