using Microsoft.AspNetCore.Components;
using Moq;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using VeraxShield.modele.utilisateurs;
using VeraxShield.services.UtilisateursDataService;

namespace TestVeraxShield;

public class UtilisateursDataServiceApiTests
{
    private readonly MockHttpMessageHandler _mockHttp;
    private readonly HttpClient _clientHttp;
    private readonly Mock<NavigationManager> _mockNavigationManager;
    private readonly UtilisateursDataServiceApi _service;

    public UtilisateursDataServiceApiTests()
    {
        _mockHttp = new MockHttpMessageHandler();

        // Mock the response for the API call
        _mockHttp.When("https://Verax.com/api/utilisateurs/recuperer")
            .Respond("application/json", JsonConvert.SerializeObject(new List<Utilisateur>
            {
                new Utilisateur("testUser", "User", "Test", "dez", "password", "User", false)
            }));

        _clientHttp = _mockHttp.ToHttpClient();
        _clientHttp.BaseAddress = new System.Uri("https://Verax.com");

        _mockNavigationManager = new Mock<NavigationManager>();
        _service = new UtilisateursDataServiceApi(_clientHttp, _mockNavigationManager.Object);
    }

    [Fact]
    public async Task GetAllUtilisateurs_ReturnsUsers()
    {
        // Act
        var users = await _service.getAllUtilisateurs();

        // Assert
        Assert.Single(users);
        Assert.Equal("testUser", users[0].Pseudo);
    }
    
    [Fact]
    public async Task GetUtilisateurFromPseudo_ReturnsUser()
    {
        // Act
        var user = await _service.getUtilisateurFromPseudo("testUser");

        // Assert
        Assert.Equal("testUser", user.Pseudo);
    }
    
}