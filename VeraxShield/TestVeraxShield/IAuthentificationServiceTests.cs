using Moq;
using VeraxShield.modele.utilisateurs;
using VeraxShield.services.UtilisateursDataService;

namespace TestVeraxShield;

public class IAuthentificationServiceTests
{
    private readonly Mock<IUtilisateursDataService> _mockDataService;
    private readonly AuthentificationService _authService;

    public IAuthentificationServiceTests()
    {
        _mockDataService = new Mock<IUtilisateursDataService>();
        _authService = new AuthentificationService(_mockDataService.Object);
    }

    [Fact]
    public async Task GetUtilisateur_ValidUser_ReturnsUser()
    {
        // Arrange
        var expectedUser = new UtilisateurCourant { Pseudo = "user1", EstAuthentifie = true };
        _mockDataService.Setup(x => x.getAllUtilisateurs()).ReturnsAsync(new List<Utilisateur>
        {
            new Utilisateur("user1", "Name", "Surname", "user1@example.com", "password", "User", false)
        });

        // Act
        var result = await _authService.GetUtilisateur("user1");

        // Assert
        Assert.Equal(expectedUser.Pseudo, result.Pseudo);
        Assert.True(result.EstAuthentifie);
    }

    [Fact]
    public async Task GetUtilisateur_NonExistentUser_ThrowsException()
    {
        // Arrange
        _mockDataService.Setup(x => x.getAllUtilisateurs()).ReturnsAsync(new List<Utilisateur>());

        // Act & Assert
        await Assert.ThrowsAsync<Exception>(() => _authService.GetUtilisateur("user2"));
    }
}