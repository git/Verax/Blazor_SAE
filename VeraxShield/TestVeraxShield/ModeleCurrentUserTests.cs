namespace TestVeraxShield;

public class ModeleCurrentUserTests
{
    [Fact]
    public void InitializesCorrectly()
    {
        var utilisateur = new UtilisateurCourant();

        Assert.Null(utilisateur.Claims);
        Assert.False(utilisateur.EstAuthentifie);
        Assert.Null(utilisateur.Pseudo);
    }

    [Fact]
    public void AddsAndUpdatesClaimsCorrectly()
    {
        var utilisateur = new UtilisateurCourant { Claims = new Dictionary<string, string>() };
        utilisateur.Claims.Add("role", "user");

        Assert.Equal("user", utilisateur.Claims["role"]);

        utilisateur.Claims["role"] = "admin";
        Assert.Equal("admin", utilisateur.Claims["role"]);
    }

    [Fact]
    public void RemovesClaimsCorrectly()
    {
        var utilisateur = new UtilisateurCourant { Claims = new Dictionary<string, string>() };
        utilisateur.Claims.Add("role", "user");
        utilisateur.Claims.Remove("role");

        Assert.False(utilisateur.Claims.ContainsKey("role"));
    }

    [Fact]
    public void TogglesAuthenticationState()
    {
        var utilisateur = new UtilisateurCourant();
        utilisateur.EstAuthentifie = true;

        Assert.True(utilisateur.EstAuthentifie);

        utilisateur.EstAuthentifie = false;
        Assert.False(utilisateur.EstAuthentifie);
    }

    [Fact]
    public void UpdatesPseudoCorrectly()
    {
        var utilisateur = new UtilisateurCourant();
        utilisateur.Pseudo = "TestUser";

        Assert.Equal("TestUser", utilisateur.Pseudo);
    }
}